# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# Copyright (c) 2014, TRIOLOGY GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# http://www.scm-manager.com

package Bugzilla::Extension::CAS::Auth::Login;
use strict;
use warnings;
use base qw(Bugzilla::Auth::Login);
use constant requires_persistence    => 1;
use constant user_can_create_account => 0;
use constant requires_verification   => 0;
use constant is_automatic            => 1;
use constant can_login               => 0;
use constant can_logout              => 0;
use Bugzilla::Constants;
use Bugzilla::Extension::CAS::Auth::AuthCASSaml;
use HTTP::Cookies;


# Always returns no data.
sub get_login_info {
	my $q = Bugzilla->cgi();
	my $ticket = $q->param('ticket') || $q->param('SAMLart');
	if ( defined($ticket) && ( !$ticket eq '' ) ) {
		return validateTicket($ticket);
	}

	my $url = CGI->new->url();
	if ( $url =~ /jsonrpc\.cgi|xmlrpc\.cgi/ ) {
		my $inputParams = Bugzilla->input_params;
        if (defined $inputParams->{'login'} && defined $inputParams->{'password'}) {
       		# API Login Call
		  return loginViaWebservice();
        } else {
        	Bugzilla->logout();
        }
	}

	return { failure => AUTH_NODATA };
}

# Not logged in. Redirect to CAS server.
sub fail_nodata {
	redirectToCasServer();
	exit;
}

sub redirectToCasServer {
    my $cas = createCasServer();
    # redirect
    my $query = Bugzilla->cgi();
    my $server = $query->url();
    print $query->redirect($cas->getServerLoginURL($server));
    # stop execution
    exit;
}

sub createCasServer {
	my $casServer = new Bugzilla::Extension::CAS::Auth::AuthCASSaml(
		casUrl        => Bugzilla->params->{'CASserver'},
		CAFile        => Bugzilla->params->{'CASserverCert'},
		samlTolerance => Bugzilla->params->{'CAStimeTolerance'},
		saml          => 1
	);
	return $casServer;
}

sub getLogoutUrl {
	my $cas     = createCasServer();
	my $service = Bugzilla->params->{'urlbase'};
	return $cas->getServerLogoutURL($service);
}

sub loginViaWebservice {
	my $params   = Bugzilla->input_params;
	my $username = $params->{login};
	my $password = $params->{password};

	# you might want to use Data::Dumper; and Dumper instead of return
	# just for looking at what is being returned here.

	# full URI to the base path for REST calls
	my $cas = Bugzilla->params->{'CASserver'} . '/v1';

	# at this point google must be tired of receiving my test STs :)
	my $service = Bugzilla->params->{'urlbase'};

	my $ua          = LWP::UserAgent->new;
	my %ssl_options = (
		verify_hostname => 0,
		SSL_ca_file     => Bugzilla->params->{'CASserverCert'}
	);
	$ua->ssl_opts(%ssl_options);

	#
	# we need a cookie jar
	$ua->cookie_jar( HTTP::Cookies->new( file => "/tmp/.cookies.txt" ) );
	my $server = createCasServer();

	# Get the TGT.
	my $response = $ua->post(
		$cas . '/tickets',
		{
			username => $username,
			password => $password
		}
	);

	return { failure => AUTH_LOGINFAILED }
	  unless $response->is_success;

	# The TGT is somewhere inside Location, but it's fine...
	# we just need to call this Location with a 'service' parameter
	# to get a valid ST -- we're already authenticated.
	$response =
	  $ua->post( $response->header('Location'), { service => $service } );

	return { failure => AUTH_ERROR }
	  unless $response->is_success;

	# The ST is back inside the content of the response.
	my $st = $response->decoded_content;

	undef $ua;
	undef $response;    # ... more housekeeping.

	# TGT is lost. It's up to the developer how to handle this cookie
	# which will grant access to a mod_auth_cas secured resource.

	return validateTicket($st);
}

1;

sub validateTicket {
	my $ticket = shift;

	my $cas     = createCasServer();
	my $service = Bugzilla->params->{'urlbase'};
	## Note that the CAS result returned is now a hash.
	my %casResult = $cas->validateST( $service, $ticket );
	
	if ( $casResult{user} ) {
		## If there were any attributes, they're stored under the attributes key
		## Note that this is a reference to the hash and must be treated as such.
		my $attrs = $casResult{attributes};

		my $email    = undef;
		my $realname = undef;
		if ($attrs) {
			$email = $attrs->{ Bugzilla->params->{'CASmailAttribute'} }{'content'};
			$realname = $attrs->{ Bugzilla->params->{'CASfullNameAttribute'} }{'content'};
		}

		if ( !defined($email) ) {
			print STDERR "E-Mail attribute did not contain a value.";
			return { failure => AUTH_ERROR };
		}
		if ( !defined($realname) ) {
			$realname = $casResult{user};
		}

		return {
			username  => $email,
			extern_id => $casResult{user},
			realname  => $realname
		};
	} 
    
    print STDERR "Invalid authentication event";
    print STDERR "Error: " . $cas->get_errors();
    return { failure => AUTH_ERROR };
}