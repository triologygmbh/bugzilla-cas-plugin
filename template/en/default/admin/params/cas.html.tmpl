[%#
  #
  # Copyright (c) 2014, TRIOLOGY GmbH
  # All rights reserved.
  #
  # Redistribution and use in source and binary forms, with or without
  # modification, are permitted provided that the following conditions are met:
  #
  # 1. Redistributions of source code must retain the above copyright notice,
  #    this list of conditions and the following disclaimer.
  # 2. Redistributions in binary form must reproduce the above copyright notice,
  #    this list of conditions and the following disclaimer in the documentation
  #    and/or other materials provided with the distribution.
  #
  # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  # AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  # IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  # DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
  # DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  # (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  # LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  # ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  # (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  # SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  #
  # http://www.scm-manager.com
  #%]
[%
    title = "CAS"
    desc = "Configure this first before choosing CAS as an authentication method."
%]

[% param_descs = {
  CASserver => "The name (and optionally port) of your CAS server " _
                "(e.g. https://localhost:8080/cas). ",
  
  CASserverCert => "Path to trusted CA store. (e.g. /etc/ssl/certs/cacert.cer)",
  
  CASprotocol => "Choose the CAS protocol. SAML 1.1 should be the best option.",

  CASfullNameAttribute => "Attribute holding the user's full name.",

  CASmailAttribute => "Attribute holding the user's email address.",

  CAStimeTolerance => "The tolerance in milliseconds for drifting clocks when validating SAML 1.1 tickets. " _
                      "Note that 10 seconds should be more than enough for most environments that have " _
                      "NTP time synchronization. Default is 1000 milliseconds." }
%]
