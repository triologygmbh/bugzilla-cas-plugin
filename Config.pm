# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# Copyright (c) 2014, TRIOLOGY GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# http://www.scm-manager.com

package Bugzilla::Extension::CAS;
use strict;
use warnings;
use constant NAME             => 'CAS';
use constant REQUIRED_MODULES => [
	{
		package => 'IO-Socket-SSL',
		module  => 'IO::Socket::SSL',
		version => 0,
	},
	{
		package => 'LWP-UserAgent',
		module  => 'LWP::UserAgent',
		version => 0,
	},
	{
		pacakge => "LWP HTTPS module",
		module => 'LWP::Protocol::https',
		version => 0,
	},
	{
		package => 'XML-Simple',
		module  => 'XML::Simple',
		version => 0,
	},
	{
		package => 'Date-Parse',
		module  => 'Date::Parse',
		version => 0,
	}
];

__PACKAGE__->NAME;
